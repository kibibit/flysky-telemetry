#ifndef BMP180_H
#define BMP180_H

//*********************************************************************************
// Headers
//*********************************************************************************
#include "twi.h"

//*********************************************************************************
// Macros and Globals
//*********************************************************************************
typedef enum {
    BMP180_ULTRALOWPOWER = 0x00, //low power             mode, oss0
    BMP180_STANDARD      = 0x01, //standard              mode, oss1
    BMP180_HIGHRES       = 0x02, //high resolution       mode, oss2
    BMP180_ULTRAHIGHRES  = 0x03  //ultra high resolution mode, oss3
} BMP180_RESOLUTION;

/* calibration registers */
typedef enum{
    BMP180_CAL_AC1_REG =                0xAA,  //ac1 pressure    computation
    BMP180_CAL_AC2_REG =                0xAC,  //ac2 pressure    computation
    BMP180_CAL_AC3_REG =                0xAE,  //ac3 pressure    computation
    BMP180_CAL_AC4_REG =                0xB0,  //ac4 pressure    computation
    BMP180_CAL_AC5_REG =                0xB2,  //ac5 temperature computation
    BMP180_CAL_AC6_REG =                0xB4,  //ac6 temperature computation
    BMP180_CAL_B1_REG  =                0xB6,  //b1  pressure    computation
    BMP180_CAL_B2_REG  =                0xB8,  //b2  pressure    computation
    BMP180_CAL_MB_REG  =                0xBA,  //mb
    BMP180_CAL_MC_REG  =                0xBC,  //mc  temperature computation
    BMP180_CAL_MD_REG  =                0xBE   //md  temperature computation
} BMP180_CAL_REG;

#define BMP180_GET_ID_REG             0xD0   //device id register
#define BMP180_GET_VERSION_REG        0xD1   //device version register

#define BMP180_SOFT_RESET_REG         0xE0   //soft reset register
#define BMP180_SOFT_RESET_CTRL        0xB6   //soft reset control

#define BMP180_START_MEASURMENT_REG   0xF4   //start measurment  register
#define BMP180_READ_ADC_MSB_REG       0xF6   //read adc msb  register
#define BMP180_READ_ADC_LSB_REG       0xF7   //read adc lsb  register
#define BMP180_READ_ADC_XLSB_REG      0xF8   //read adc xlsb register

/* BMP180_START_MEASURMENT_REG controls */
#define BMP180_GET_TEMPERATURE_CTRL   0x2E   //get temperature control
#define BMP180_GET_PRESSURE_OSS0_CTRL 0x34   //get pressure oversampling 1 time/oss0 control
#define BMP180_GET_PRESSURE_OSS1_CTRL 0x74   //get pressure oversampling 2 time/oss1 control
#define BMP180_GET_PRESSURE_OSS2_CTRL 0xB4   //get pressure oversampling 4 time/oss2 control
#define BMP180_GET_PRESSURE_OSS3_CTRL 0xF4   //get pressure oversampling 8 time/oss3 control

/* misc */
#define BMP180_ADDRESS                0x77   //i2c address
#define BMP180_CHIP_ID                0x55   //id number

#define BMP180_ERROR                  255    //returns 255, if communication error is occurred

/* to store calibration coefficients */
typedef struct{
    int16_t  bmpAC1;
    int16_t  bmpAC2;
    int16_t  bmpAC3;
    uint16_t bmpAC4;
    uint16_t bmpAC5;
    uint16_t bmpAC6;

    int16_t  bmpB1;
    int16_t  bmpB2;

    int16_t  bmpMB;
    int16_t  bmpMC;
    int16_t  bmpMD;
} BMP180_CAL_COEFF;

//*********************************************************************************
// Prototypes
//*********************************************************************************
void BMP180_init(void);
int32_t BMP180_getPressure(void);
float BMP180_getTemperature(void);

void BMP180_readCalibrationCoefficients(void);
uint16_t BMP180_readRawTemperature(void);
uint32_t BMP180_readRawPressure(void);
int32_t BMP180_computeB5(int32_t UT);

uint8_t read8(uint8_t reg);
uint16_t read16(uint8_t reg);
void write8(uint8_t reg, uint8_t control);

#endif
