#ifndef SERIAL_H
#define SERIAL_H

//*********************************************************************************
// Headers
//*********************************************************************************
#include <avr/io.h>
#include <avr/interrupt.h>

//*********************************************************************************
// Macros and Globals
//*********************************************************************************
#define USART0_BAUD_RATE(BAUD_RATE) ((float)(F_CPU * 64 / (16 * (float)BAUD_RATE)) + 0.5)

#define UART_TX_BUFFER_SIZE  16
#define UART_TX_BUFFER_MASK (UART_TX_BUFFER_SIZE - 1)
#if (UART_TX_BUFFER_SIZE & UART_TX_BUFFER_MASK) != 0
#error UART_TX_BUFFER_SIZE must be a power of two and <= 256
#endif

struct UART_TX_BUFFER
{
    volatile uint8_t head;
    volatile uint8_t tail;
    uint8_t buf[UART_TX_BUFFER_SIZE];
};

//*********************************************************************************
// Prototypes
//*********************************************************************************
void USART0_init(void);

void UART0TxByte(const uint8_t data);

#endif