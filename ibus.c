#include "ibus.h"

void IBUS_init(Ibus *ibus){
    ibus->state = DISCARD;
    ibus->chksum = 0;
    ibus->lchksum = 0;
    ibus->num_sensors=0;
    ibus_run = ibus;
}

uint8_t IBUS_addSensor(Ibus *ibus, uint8_t sensor_type, uint8_t len){
    sensors[ibus->num_sensors].sensorType = sensor_type;
    sensors[ibus->num_sensors].sensorLength = len;
    sensors[ibus->num_sensors].sensorValue = 0;
    ibus->num_sensors++;
    return ibus->num_sensors;
}

uint8_t IBUS_processIncomingData(Ibus *ibus, uint8_t data){
    uint8_t chksum_ok = 0;
    if(ibus->state == DISCARD) {
        ibus->state = GET_LENGTH;
    }
    switch (ibus->state){
        case GET_LENGTH:
            if(data == 0x04){
                ibus->chksum = 0xFFFF - data;
                ibus->state = GET_DATA;
            }else{
                ibus->state = DISCARD;
            }
            break;
        
        case GET_DATA:
            ibus->command = data;
            ibus->chksum -= data;
            ibus->state = GET_CHKSUML;
            break;
        
        case GET_CHKSUML:
            ibus->lchksum = data;
            ibus->state = GET_CHKSUMH;
            break;
        
        case GET_CHKSUMH:
            if (ibus->chksum == (data << 8) + ibus->lchksum){
                chksum_ok = ibus->command & 0x0F;
            }
            ibus->state = DISCARD;
            break;
        
        case DISCARD:
        default:
            break;
    }
    return chksum_ok;
}

void IBUS_sendData(Ibus *ibus, uint8_t adr){
    SensorInfo *s = &sensors[adr-1];
    uint8_t t;
    switch (ibus->command & 0x0F0) {
        case PROTOCOL_COMMAND_DISCOVER: // 0x80, discover sensor
            UART0TxByte(0x04);
            UART0TxByte(ibus->command);
            ibus->chksum = 0xFFFF - (0x04 + ibus->command);
            break;

        case PROTOCOL_COMMAND_TYPE: // 0x90, send sensor type
            UART0TxByte(0x06);
            UART0TxByte(ibus->command);
            UART0TxByte(s->sensorType);
            UART0TxByte(s->sensorLength);
            ibus->chksum = 0xFFFF - (0x06 + ibus->command + s->sensorType + s->sensorLength);
            break;

        case PROTOCOL_COMMAND_VALUE: // 0xA0, send sensor data
            UART0TxByte(t = 0x04 + s->sensorLength);
            ibus->chksum = 0xFFFF - t;
            UART0TxByte(ibus->command);
            ibus->chksum -= ibus->command;
            UART0TxByte(t = s->sensorValue & 0x0ff);
            ibus->chksum -= t;
            UART0TxByte(t = (s->sensorValue >> 8) & 0x0ff); 
            ibus->chksum -= t;
            if (s->sensorLength==4) {
                UART0TxByte(t = (s->sensorValue >> 16) & 0x0ff); 
                ibus->chksum -= t;
                UART0TxByte(t = (s->sensorValue >> 24) & 0x0ff); 
                ibus->chksum -= t;                  
            }
            break;

        default:
            break;
    }
    UART0TxByte(ibus->chksum & 0x0ff);
    UART0TxByte(ibus->chksum >> 8);
}

void IBUS_setSensorMeasurement(uint8_t adr, int32_t value){
    sensors[adr-1].sensorValue = value;
}

ISR(USART0_RXC_vect){
    uint8_t chksum_ok = IBUS_processIncomingData(ibus_run, USART0.RXDATAL);
    if(chksum_ok >= 1 && chksum_ok <= ibus_run->num_sensors){
        IBUS_sendData(ibus_run, chksum_ok);
        USART0.CTRLA = USART_DREIE_bm | USART_LBME_bm;   // Enable UART0 Tx interrupt
    }
}