# Telemetry Device with ATtiny1616 for FlySky Radio

Ibus implementation written in c that allow send sensor data to a radio control system.

This project was tested with FS-IA6B receiver and FS-I6X transceiver.

## Getting Started

to use this code you can compile y flash an ATtiny1616 with the commands given below defined in the Makefile

```
make all
make flash
```

I used avr-gcc for compile and [pymcuprog](https://pypi.org/project/pymcuprog/) for flash the microcontroller.

## Ibus Implementation

The protocol implementation was written based on [IbusBM library for Arduino](https://github.com/bmellink/IBusBM) and Ibus specification available [here](https://github.com/betaflight/betaflight/wiki/Single-wire-FlySky-(IBus)-telemetry), ibus runs in background using interrupts to ensure the perfect timing required.

This implementation requires a dedicated UART hardware Serial port and has **intended for only send sensors data not receive servo commands**.

### Telemetry Sensors

Sensors defined in this project are defined in ibus.h
```
#define IBUS_TEMP 0x01 // Temperature (in 0.1 degrees, where 0=-40'C)
#define IBUS_RPM  0x02 // RPM
#define IBUS_EXTV 0x03 // External voltage (in 0.01)
#define IBUS_PRESS 0x41 // Pressure (in Pa)
```

### UART Configuration

The UART was configured in single wire mode, with a baudrate of 115200 and using interrupts for receive and send data. One circular buffer for send data was added, the buffer size is specified in serial.h and has a size of 16 bytes.
The port multiplexor is used so the pin used for the ibus communication is PortA1 instead of PB2.

### Background Processing

This is how the ibus run in background and the timing required has acomplished.

- Almost all the time the microcontroller only has the receive interrupt enabled.

- When a command was received and validated the tx_buffer is loaded with the data to send.

- When the buffer was loaded the interrupt for sending data was enabled and the receive interrupt is disabled.

- Finally when the data was sent the interrupt for sending data was disabled and the receive interrupt is enabled.
