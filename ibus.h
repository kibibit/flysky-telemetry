#ifndef IBUS_H
#define IBUS_H

//*********************************************************************************
// Headers
//*********************************************************************************
#include "serial.h"

//*********************************************************************************
// Macros and Globals
//*********************************************************************************
#define IBUS_TEMP 0x01 // Temperature (in 0.1 degrees, where 0=-40'C)
#define IBUS_RPM  0x02 // RPM
#define IBUS_EXTV 0x03 // External voltage (in 0.01)
#define IBUS_PRESS 0x41 // Pressure (in Pa)

#define PROTOCOL_COMMAND_DISCOVER 0x80
#define PROTOCOL_COMMAND_TYPE 0x90
#define PROTOCOL_COMMAND_VALUE 0xA0

enum State {GET_LENGTH, GET_DATA, GET_CHKSUML, GET_CHKSUMH, DISCARD};

typedef struct {
    uint8_t sensorType;
    uint8_t sensorLength;
    uint32_t sensorValue;
} SensorInfo;

SensorInfo sensors[4];

typedef struct {
    enum State state;
    uint8_t command;
    uint16_t chksum;
    uint8_t lchksum;
    uint8_t num_sensors;
} Ibus;

Ibus *ibus_run;

//*********************************************************************************
// Prototypes
//*********************************************************************************
void IBUS_init(Ibus *ibus);

uint8_t IBUS_addSensor(Ibus *ibus, uint8_t sensor_type, uint8_t len);

uint8_t IBUS_processIncomingData(Ibus *ibus, uint8_t data);

void IBUS_sendData(Ibus *ibus, uint8_t adr);

void IBUS_setSensorMeasurement(uint8_t adr, int32_t value);

#endif