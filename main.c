/*
 * Flysky Telemetry Sensor
 * V1.0
*/

#include <avr/io.h>
#include "serial.h"
#include "adc.h"
#include "ibus.h"
#include "BMP180.h"
#include <util/delay.h>

uint16_t adcVal;
int32_t pressure;
Ibus ibus;

int main(void){
    _PROTECTED_WRITE(CLKCTRL.MCLKCTRLB, CLKCTRL_CLKSEL_OSC20M_gc);
    ADC0_init();
    USART0_init();
    BMP180_init();
    IBUS_init(&ibus);
    IBUS_addSensor(&ibus, IBUS_EXTV, 2);
    IBUS_addSensor(&ibus, IBUS_PRESS, 4);
    sei();
    
    while (1) {
        adcVal = ADC0_read() * 2.674674;
        pressure = BMP180_getPressure();
        IBUS_setSensorMeasurement(1, adcVal);
        IBUS_setSensorMeasurement(2, pressure);
        _delay_ms(1000);
    }
    return 1;
}
