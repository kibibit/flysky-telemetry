#include "serial.h"

struct UART_TX_BUFFER U0Buf;

void UART0TxByte(const uint8_t data){
    const uint8_t tmphead = (U0Buf.head + 1) & UART_TX_BUFFER_MASK;
    U0Buf.buf[tmphead] = data;
    U0Buf.head = tmphead;
}

void USART0_init(void){
   PORTMUX.CTRLB = PORTMUX_USART0_ALTERNATE_gc;

   U0Buf.head = 0;
   U0Buf.tail = 0;
   USART0.BAUD = (uint16_t)USART0_BAUD_RATE(115200);
   USART0.CTRLB = USART_RXEN_bm | USART_TXEN_bm | USART_RXMODE_NORMAL_gc | USART_ODME_bm;
   USART0.CTRLA = USART_RXCIE_bm | USART_LBME_bm;
}

ISR(USART0_DRE_vect){
    if (U0Buf.head != U0Buf.tail){ // Is there anything to send?
        const uint8_t tmptail = (U0Buf.tail + 1) & UART_TX_BUFFER_MASK;
        U0Buf.tail = tmptail;
        USART0.TXDATAL = U0Buf.buf[tmptail];    // Transmit one byte
    }
    else{
        USART0.CTRLA = USART_RXCIE_bm | USART_LBME_bm; // Nothing left to send; disable Tx interrupt
    }
}