#ifndef TWI_H
#define TWI_H

//*********************************************************************************
// Headers
//*********************************************************************************
#include <avr/io.h>

//*********************************************************************************
// Macros and Globals
//*********************************************************************************
#define F_CPU 20000000UL
// 400kHz clock
#define FREQ 400000UL
#define T_RISE 300UL      // Rise time
// Choose these for 1MHz clock
// #define T_RISE 120UL        // Rise time
#define TWI0_BAUD_RATE() ((F_CPU/FREQ - (F_CPU*T_RISE/1000000000) - 10)/2)

uint8_t I2Ccount;

//*********************************************************************************
// Prototypes
//*********************************************************************************
void TWI0_init(void);

uint8_t TWI0_start(uint8_t address, uint8_t readcount);
uint8_t TWI0_restart(uint8_t address, uint8_t readcount);
void TWI0_stop(void);
uint8_t TWI0_read(void);
uint8_t TWI0_readLast(void);
uint8_t TWI0_write (uint8_t data);

#endif
