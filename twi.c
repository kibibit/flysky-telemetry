#include "twi.h"

void TWI0_init(void) {
    //PORTB.DIRSET = PIN1_bm | PIN2_bm; //disable this line if use pullup 
    TWI0.CTRLA = TWI_SDAHOLD_50NS_gc;
    TWI0.MSTATUS = TWI_BUSSTATE_IDLE_gc;
    TWI0.MBAUD = (uint8_t)TWI0_BAUD_RATE();
    TWI0.MCTRLA = TWI_ENABLE_bm;
}

uint8_t TWI0_start(uint8_t address, uint8_t readcount) {
    uint8_t read = 0;
    if (readcount != 0) { I2Ccount = readcount; read = 1; }                        // Read
    TWI0.MADDR = address<<1 | read;                                 // Send START condition
    while (!(TWI0.MSTATUS & (TWI_WIF_bm | TWI_RIF_bm)));            // Wait for write or read interrupt flag
    if (TWI0.MSTATUS & TWI_ARBLOST_bm) {                            // Arbitration lost or bus error
        while (!(TWI0.MSTATUS & TWI_BUSSTATE_IDLE_gc));               // Wait for bus to return to idle state
        return 0;
    } else if (TWI0.MSTATUS & TWI_RXACK_bm) {                       // Address not acknowledged by client
        TWI0.MCTRLB |= TWI_MCMD_STOP_gc;                              // Send stop condition
        while (!(TWI0.MSTATUS & TWI_BUSSTATE_IDLE_gc));               // Wait for bus to return to idle state
        return 0;
    }
    return 1; 
}

uint8_t TWI0_restart(uint8_t address, uint8_t readcount) {
    return TWI0_start(address, readcount);
}

void TWI0_stop(void) {
    TWI0.MCTRLB |= TWI_MCMD_STOP_gc;                                // Send STOP
    while (!(TWI0.MSTATUS & TWI_BUSSTATE_IDLE_gc));                 // Wait for bus to return to idle state
}

uint8_t TWI0_read(void) {
    if (I2Ccount != 0) I2Ccount--;
    while (!(TWI0.MSTATUS & TWI_RIF_bm));                           // Wait for read interrupt flag
    uint8_t data = TWI0.MDATA;
    // Check slave sent ACK?
    if (I2Ccount != 0) TWI0.MCTRLB = TWI_MCMD_RECVTRANS_gc;         // ACK = more bytes to read
    else TWI0.MCTRLB = TWI_ACKACT_NACK_gc;                          // Send NAK
    return data;
}

uint8_t TWI0_readLast (void) {
    I2Ccount = 0;
    return TWI0_read();
}

uint8_t TWI0_write (uint8_t data) {
    TWI0.MCTRLB = TWI_MCMD_RECVTRANS_gc;                            // Prime transaction
    TWI0.MDATA = data;                                              // Send data
    while (!(TWI0.MSTATUS & TWI_WIF_bm));                           // Wait for write to complete
    if (TWI0.MSTATUS & (TWI_ARBLOST_bm | TWI_BUSERR_bm)) return 0; // Fails if bus error or arblost
    return !(TWI0.MSTATUS & TWI_RXACK_bm);                          // Returns true if slave gave an ACK
}
