PRG            = main
OBJ            = main.o serial.o adc.o ibus.o twi.o BMP180.o
F_CPU          = 20000000UL
MCU_TARGET     = attiny1616
OPTIMIZE       = -Os

DEFS           = -DF_CPU=$(F_CPU)
LIBS           =

PORT           = /dev/ttyUSB0

# You should not have to change anything below here.

CC             = avr-gcc

CFLAGS        = -g -Wall $(OPTIMIZE) -mmcu=$(MCU_TARGET) $(DEFS)
LDFLAGS       = -Wl,-Map,$(PRG).map

OBJCOPY        = avr-objcopy
OBJDUMP        = avr-objdump
OBJSIZE        = avr-size

#all: $(PRG).elf lst text eeprom
all: $(PRG).elf lst text size

$(PRG).elf: $(OBJ)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LIBS)

clean:
	rm -rf *.o $(PRG).elf *.eps *.png *.pdf *.bak
	rm -rf *.lst *.map $(EXTRA_CLEAN_FILES)

test:
	pymcuprog ping -d $(MCU_TARGET) -t uart -u $(PORT)

flash:
	pymcuprog write -f $(PRG).hex --erase --verify -d $(MCU_TARGET) -t uart -u $(PORT)

size:
	$(OBJSIZE) $(PRG).elf

lst:  $(PRG).lst

%.lst: %.elf
	$(OBJDUMP) -h -S $< > $@

# Rules for building the .text rom images

#text: hex bin srec
text: hex

hex:  $(PRG).hex
bin:  $(PRG).bin
srec: $(PRG).srec

# $(OBJCOPY) -j .text -j .data -O ihex $< $@
%.hex: %.elf
	$(OBJCOPY) -O ihex $< $@

%.srec: %.elf
	$(OBJCOPY) -j .text -j .data -O srec $< $@

%.bin: %.elf
	$(OBJCOPY) -j .text -j .data -O binary $< $@
