#include "BMP180.h"
#include <math.h>
#include <util/delay.h>

uint8_t resolution = BMP180_ULTRAHIGHRES;
BMP180_CAL_COEFF calCoeff;

void BMP180_init(void){
    TWI0_init();
    BMP180_readCalibrationCoefficients();
}

int32_t BMP180_getPressure(void){
    int32_t  UT       = 0;
    int32_t  UP       = 0;
    int32_t  B3       = 0;
    int32_t  B5       = 0;
    int32_t  B6       = 0;
    int32_t  X1       = 0;
    int32_t  X2       = 0;
    int32_t  X3       = 0;
    int32_t  pressure = 0;
    uint32_t B4       = 0;
    uint32_t B7       = 0;

    UT = BMP180_readRawTemperature();                                            //read uncompensated temperature, 16-bit
    if (UT == BMP180_ERROR) return BMP180_ERROR;                          //error handler, collision on i2c bus

    UP = BMP180_readRawPressure();                                               //read uncompensated pressure, 19-bit
    if (UP == BMP180_ERROR) return BMP180_ERROR;                          //error handler, collision on i2c bus

    B5 = BMP180_computeB5(UT);

    /* pressure calculation */
    B6 = B5 - 4000;
    X1 = ((int32_t)calCoeff.bmpB2 * ((B6 * B6) >> 12)) >> 11;
    X2 = ((int32_t)calCoeff.bmpAC2 * B6) >> 11;
    X3 = X1 + X2;
    B3 = ((((int32_t)calCoeff.bmpAC1 * 4 + X3) << resolution) + 2) / 4;

    X1 = ((int32_t)calCoeff.bmpAC3 * B6) >> 13;
    X2 = ((int32_t)calCoeff.bmpB1 * ((B6 * B6) >> 12)) >> 16;
    X3 = ((X1 + X2) + 2) >> 2;
    B4 = ((uint32_t)calCoeff.bmpAC4 * (X3 + 32768L)) >> 15;
    B7 = (UP - B3) * (50000UL >> resolution);

    if (B4 == 0) return BMP180_ERROR;                                     //safety check, avoiding division by zero

    if   (B7 < 0x80000000) pressure = (B7 * 2) / B4;
    else                   pressure = (B7 / B4) * 2;

    X1 = pow((pressure >> 8), 2);
    X1 = (X1 * 3038L) >> 16;
    X2 = (-7357L * pressure) >> 16;

    return pressure = pressure + ((X1 + X2 + 3791L) >> 4);
}

float BMP180_getTemperature(void){
    int16_t rawTemperature = BMP180_readRawTemperature();
    if (rawTemperature == BMP180_ERROR) return BMP180_ERROR;   //error handler, collision on i2c bus
    return (float)((BMP180_computeB5(rawTemperature) + 8) >> 4) / 10;
}

void BMP180_readCalibrationCoefficients(void){
    int32_t value = 0;
    for (uint8_t reg = BMP180_CAL_AC1_REG; reg <= BMP180_CAL_MD_REG; reg++){
        value = read16(reg);
        switch (reg){
            case BMP180_CAL_AC1_REG:               //used for pressure computation
            calCoeff.bmpAC1 = value;
            break;

            case BMP180_CAL_AC2_REG:               //used for pressure computation
            calCoeff.bmpAC2 = value;
            break;

            case BMP180_CAL_AC3_REG:               //used for pressure computation
            calCoeff.bmpAC3 = value;
            break;

            case BMP180_CAL_AC4_REG:               //used for pressure computation
            calCoeff.bmpAC4 = value;
            break;

            case BMP180_CAL_AC5_REG:               //used for temperature computation
            calCoeff.bmpAC5 = value;
            break;

            case BMP180_CAL_AC6_REG:               //used for temperature computation
            calCoeff.bmpAC6 = value;
            break;

            case BMP180_CAL_B1_REG:                //used for pressure computation
            calCoeff.bmpB1 = value;
            break;

            case BMP180_CAL_B2_REG:                //used for pressure computation
            calCoeff.bmpB2 = value;
            break;

            case BMP180_CAL_MB_REG:                //???
            calCoeff.bmpMB = value;
            break;

            case BMP180_CAL_MC_REG:                //used for temperature computation
            calCoeff.bmpMC = value;
            break;

            case BMP180_CAL_MD_REG:                //used for temperature computation
            calCoeff.bmpMD = value;
            break;
        }
    }
}

uint16_t BMP180_readRawTemperature(void){
    /* send temperature measurement command */
    write8(BMP180_START_MEASURMENT_REG, BMP180_GET_TEMPERATURE_CTRL);
    /* set measurement delay */
    _delay_ms(5);
    /* read result */
    return read16(BMP180_READ_ADC_MSB_REG);                                                            //reads msb + lsb
}

uint32_t BMP180_readRawPressure(void){
    uint8_t  regControl  = 0;
    uint32_t rawPressure = 0;

    /* convert resolution to register control */
    switch (resolution){
        case BMP180_ULTRALOWPOWER:                    //oss0
            regControl = BMP180_GET_PRESSURE_OSS0_CTRL;
            break;

        case BMP180_STANDARD:                         //oss1
            regControl = BMP180_GET_PRESSURE_OSS1_CTRL;
            break;

        case BMP180_HIGHRES:                          //oss2
            regControl = BMP180_GET_PRESSURE_OSS2_CTRL;
            break;

        case BMP180_ULTRAHIGHRES:                     //oss3
            regControl = BMP180_GET_PRESSURE_OSS3_CTRL;
            break;
    }

    /* send pressure measurement command */
    write8(BMP180_START_MEASURMENT_REG, regControl);
    /* set measurement delay */
    switch (resolution){
        case BMP180_ULTRALOWPOWER:
            _delay_ms(5);
            break;

        case BMP180_STANDARD:
            _delay_ms(8);
            break;

        case BMP180_HIGHRES:
            _delay_ms(14);
            break;

        case BMP180_ULTRAHIGHRES:
            _delay_ms(26);
            break;
    }

    /* read result msb + lsb */
    rawPressure = read16(BMP180_READ_ADC_MSB_REG);        //16-bits
    if (rawPressure == BMP180_ERROR) return BMP180_ERROR; //error handler, collision on i2c bus

    /* read result xlsb */
    rawPressure <<= 8;
    rawPressure |= read8(BMP180_READ_ADC_XLSB_REG);       //19-bits

    rawPressure >>= (8 - resolution);

    return rawPressure;
}

int32_t BMP180_computeB5(int32_t UT){
    int32_t X1 = ((UT - (int32_t)calCoeff.bmpAC6) * (int32_t)calCoeff.bmpAC5) >> 15;
    int32_t X2 = ((int32_t)calCoeff.bmpMC << 11) / (X1 + (int32_t)calCoeff.bmpMD);
    return X1 + X2;
}

uint8_t read8(uint8_t reg){
    uint8_t value = 0;
    TWI0_start(BMP180_ADDRESS, 0);
    TWI0_write(reg);
    TWI0_restart(BMP180_ADDRESS, 1);
    value = TWI0_read();
    TWI0_stop();
    return value;
}

uint16_t read16(uint8_t reg){
    uint16_t value = 0;
    TWI0_start(BMP180_ADDRESS, 0);
    TWI0_write(reg);
    TWI0_restart(BMP180_ADDRESS, 2);
    value = TWI0_read() << 8;
    value |= TWI0_read();
    TWI0_stop();
    return value;
}

void write8(uint8_t reg, uint8_t control){
    TWI0_start(BMP180_ADDRESS, 0);
    TWI0_write(reg);
    TWI0_write(control);
    TWI0_stop();
}
